[![Translatorbro](https://i.imgur.com/MRhEeIY.png)](https://www.discord.com/)


# A Forever Free Discord Translator Bot - Translatorbro

Translatorbro was created out of frustration, when all the "freely" available Discord translation bots wanted to charge me money at some point.

## How to use

Simply type "!t your message" and the bot will reply with the message translated to your destination language in the same channel. All other messages that are sent in the original language will be translated to the destination language in translation specific channel.

## How to install on your server

These instructions will get you a copy of the project up and running on your server.

### Prerequisites
Make sure you have installed all of the following prerequisites on your local machine:

* Python 3.6+ [Download & Install](https://www.python.org/downloads/)
* googletrans 2.4.0 [Download & Install](https://pypi.org/project/googletrans/)
* emoji 0.5.3 [Download & Install](https://pypi.org/project/emoji/)

### Installation

1. Clone this repo to your local machine using.
```
git clone https://gitlab.com/jadhallab/translatorbro.git
```
2. Make sure all the requirements are properly installed.
```
pip install -r requirements.txt
```
3. Create a Discord bot and retrieve token key: [Tutorial](https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/)

4. Create a channel on your server where the bot can send translated messages to, and retrieve the channel ID: [Tutorial](https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord)

5. Edit 'run.py' file to add the language you wanna translate from (original language) and the language you wanna translate to (destination language). Also add the channel ID and token key for your bot.

6. Run it! If you want to host your bot 24/7 for free, checkout this [tutorial video](https://www.youtube.com/watch?v=BPvg9bndP1U).
```
python run.py
```
