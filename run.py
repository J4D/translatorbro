from googletrans import Translator
import discord
import emoji

translatorbro = Translator()

#used to filter out emojis in a message
def filter_emojis(message):
    filtered_characters = []
    character_list = list(message)
    for character in character_list:
        if character not in emoji.UNICODE_EMOJI:
            filtered_characters.append(character)
    sentence = ''.join(filtered_characters)
    return sentence

class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
        
    async def on_ready(self):
        game = discord.Game("!git install")
        await client.change_presence(status=discord.Status.online, activity=game)

    async def on_message(self, message):
        # we do not want the bot to reply to itself
        if message.author.id == self.user.id:
            return

        if message.content.startswith('!git install'): #prefix
            install_information = 'To get a copy of me up and running on your server, please refer to: https://gitlab.com/jadhallab/translatorbro'
            await message.channel.send(install_information)

        #translates only messages containing prefix into same channel
        if message.content.startswith('!t'): #prefix
            original_message = message.content[2:]
            filtered_message = filter_emojis(original_message)
            translated = translatorbro.translate(filtered_message, dest='de') #CHANGE ORIGINAL LANGUAGE HERE
            await message.channel.send(translated.text)

        #translates all messages and sends them into a translation specific channel
        elif message.content.startswith(''):
            #attachment detection
            if len(message.attachments) > 0:
                translated_string = "[Attachment]"

            else:
                original_message = message.content
                filtered_sentence = filter_emojis(original_message)
                translated_sentence = translatorbro.translate(filtered_sentence, dest='en') #CHANGE DESTINATION LANGUAGE HERE
                translated_string = translated_sentence.text
                
            author = message.author.name
            full_translation = author + ": " + translated_string
            channel = client.get_channel(608769149097344742) #CHANGE TRANSLATION CHANNEL ID HERE
            await channel.send(full_translation)

client = MyClient()
client.run('NTg0Mzc0MjAo3j0jsnhMTM5.XVxKaQ.qonssjkskNBe0dMv5vmlIOs7') #CHANGE TOKEN KEY FOR YOUR BOT HERE
